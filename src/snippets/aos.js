import { useEffect } from 'react';
import '../App.css';
import Aos from 'aos';
import 'aos/dist/aos.css';

function App() {
  useEffect(() => {
    Aos.init({ duration: 2000 })
  }, [])


  return (
    <div className="App">
      <h1>Pricing</h1>
      <div className="grid">
        <div className='boxes'>1</div>
        <div className='boxes'>2</div>
        <div className='boxes'>3</div>
        <div 
          className='boxes'
          data-aos="fade-left"
          // data-aos-offset="300"
          // data-aos-delay="50"
          // data-aos-duration="1000"
          // data-aos-easing="ease-in-out"
          // data-aos-once="true"
          // data-aos-anchor-placement="bottom-top"
        >4</div>
        <div data-aos="fade-right" className='boxes'>5</div>
        <div data-aos="fade-left" className='boxes'>6</div>
        <div data-aos="fade-right" className='boxes'>7</div>
        <div data-aos="fade-left" className='boxes'>8</div>
        <div data-aos="fade-right" className='boxes'>9</div>
        <div data-aos="fade-left" className='boxes'>10</div>
      </div>
    </div>
  );
}

export default App;
