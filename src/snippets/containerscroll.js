import { useState, useEffect, useRef } from 'react';
import '../App.css';

function Box({ element, offset, containerScroll }) {
    const box = useRef(null);
    const [top, setTop] = useState(null)

    useEffect(() => {
        setTop(box.current.getBoundingClientRect().top - offset);
    }, [offset])

    return (
        <>
            <div ref={box} className={(containerScroll > top)? 'boxes yellow':'boxes'}>{ element }</div>
        </>
    )
}

function Onscroll() {
    const [offset, setOffset] = useState(null);
    const [containerScroll, setContainerScroll] = useState(null);

    useEffect(() => {
        const pg = document.querySelector('.innerDiv');
        setOffset(pg.getBoundingClientRect().top);
        setContainerScroll(pg.clientHeight + pg.scrollTop);

        pg.addEventListener("scroll", () => {
            setContainerScroll(pg.clientHeight + pg.scrollTop);
        });
    }, [])

    const arr = [1,2,3,4,5,6]

    return (
        <div className='container'>
            <div className='innerDiv'>
                {arr.map((element) => (
                    <Box 
                        key={element} 
                        offset={offset} 
                        element={element}
                        containerScroll={containerScroll}
                    />
                ))}
            </div>
        </div>
    );
}

export default Onscroll;
