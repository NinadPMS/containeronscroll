import AosCustom from './snippets/containerscroll';

function App() {
  return (
    <div className="App">
      <AosCustom />
    </div>
  );
}

export default App;
